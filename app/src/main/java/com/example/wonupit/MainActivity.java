package com.example.wonupit;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private List<Model> modelList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ModelAdapter mAdapter;
    String Test;
    String ZONES_NAME[];
    SharedPreferences sharedPreferencesForId,
            sharedPreferencesForColor,
            sharedPreferencesForTitle,
            sharedPreferencesForFlag,
            sharedPreferencesForTime,
            sharedPreferencesForShortText,
            sharedPreferencesForArrow;

    SharedPreferences.Editor
            editorForId,
            editorForTitle,
            flageditor,
            timeeditor,
            shorttexteditor,
            colorEditor,
            arrowEditor;

    SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferencesForArrow = getSharedPreferences("arrow", MODE_PRIVATE);
        arrowEditor = sharedPreferencesForArrow.edit();


        ZONES_NAME = new String[]{"IST", "GMT", "EST"};
        sharedPreferencesForColor = getSharedPreferences("colorpref", MODE_PRIVATE);
        sharedPreferencesForId = getSharedPreferences("idpref", MODE_PRIVATE);
        sharedPreferencesForTitle = getSharedPreferences("titlepref", MODE_PRIVATE);
        sharedPreferencesForFlag = getSharedPreferences("flagpref", MODE_PRIVATE);
        sharedPreferencesForShortText = getSharedPreferences("shortTextpref", MODE_PRIVATE);
        sharedPreferencesForTime = getSharedPreferences("timepref", MODE_PRIVATE);

        editorForId = sharedPreferencesForId.edit();
        editorForTitle = sharedPreferencesForTitle.edit();
        flageditor = sharedPreferencesForFlag.edit();
        timeeditor = sharedPreferencesForTime.edit();
        colorEditor = sharedPreferencesForColor.edit();
        shorttexteditor = sharedPreferencesForShortText.edit();


        recyclerView = findViewById(R.id.postRecyclerView);
        mAdapter = new ModelAdapter(modelList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                Intent intent = new Intent(getApplicationContext(), Thread_Activity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("idb", position);
                intent.putExtras(bundle);
                startActivity(intent);
            }

            @Override
            public void onItemDoubleClicked(RecyclerView recyclerView, int position, View v) {

            }
        });


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("WonUpIt Test App");
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPreferencesForFlag.getString("flag", "yes").equals("yes")) {


            prepareData();
        } else if (sharedPreferencesForFlag.getString("flag", "no").equals("no")) {
            for (int i = 0; i < 25; i++) {


                Model tempModel = new Model(sharedPreferencesForTitle.getString("" + i, "a"),
                        sharedPreferencesForShortText.getString("" + i, "a"),
                        sharedPreferencesForTime.getString("" + i, "a"), sharedPreferencesForColor.getInt("" + i, 1),
                        i);


                modelList.add(tempModel);


                mAdapter.notifyDataSetChanged();

                if (i == 24) {
                    flageditor.putString("flag", "yes");
                    flageditor.commit();
                }
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void prepareData() {
        for (int i = 0; i < 25; i++) {
            Random rn = new Random();
            int long_string_custom = 0 + (int) (Math.random() * ((40 - 3) + 1));
            int short_string_custom = (int) (Math.random() * ((80 - 2) + 1)) + 1;
            String randomText = getAlphaNumericString(long_string_custom).trim();

            String shortText = getAlphaNumericString(short_string_custom).trim();

            int color = Color.argb(255, rn.nextInt(256), rn.nextInt(256), rn.nextInt(256));

            int mm, hr, cor = 0;
            String cord, time;
            cor = 0 + (int) (Math.random() * ((2 - 0) + 1));
            mm = (int) (Math.random() * ((59 - 1) + 1)) + 1;
            hr = (int) (Math.random() * ((12 - 1) + 1)) + 1;

            cord = ZONES_NAME[cor];

            time = "" + hr + ":" + mm + " " + cord;
            Model model = new Model(randomText, shortText, time, color, i);
            colorEditor.putInt("" + i, color);
            editorForTitle.putString("" + i, randomText);
            shorttexteditor.putString("" + i, shortText);
            timeeditor.putString("" + i, time);

            colorEditor.commit();
            editorForTitle.commit();
            shorttexteditor.commit();
            timeeditor.commit();
            modelList.add(model);


            mAdapter.notifyDataSetChanged();
        }
    }


    static String getAlphaNumericString(int n) {

        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ "

                + "abcdefghijklmnopqrstuvxyz ";

        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {


            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

    }


}