package com.example.wonupit;

import android.animation.Animator;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.text.TextPaint;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Thread_Activity extends AppCompatActivity {
    HorizontalScrollView horizontalScrollViewThumbnail, horizontalScrollViewPost;
    SharedPreferences sharedPreferences,
            sharedPreferencesForFlag,
            sharedPreferencesForColor,
            sharedPreferencesForArrow;
    SharedPreferences.Editor flagEditor, arrowEditor;

    ArrayList<Model> data = new ArrayList<>();
    Bundle receivedBundle;
    int idb;
    private List<Model> modelList = new ArrayList<>();
    private List<Model> modelListPost = new ArrayList<>();
    private ThumbnailAdapter mAdapter;
    PostThumbnailAdapter postAdapter;
    TextView rgbTextView, rgbResult;
    LinearLayout footerLayout;
    int rgb[][];
    int color;
    int flag_position = -1;
    int final_position;
    int rr, gg, bb;
    SharedPreferences.Editor colorEditor;
    RecyclerView horizontalThumbnailRecycler, postThumbnailRecycler;
    int footer_flag = 0;
    TextView hexTextView;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread_);

        sharedPreferencesForArrow = getSharedPreferences("arrow", MODE_PRIVATE);
        arrowEditor = sharedPreferencesForArrow.edit();
        arrowEditor.putInt("arrowflag", -1);
        arrowEditor.commit();

        rgbResult = findViewById(R.id.textView5);

        footer_flag = 0;
        final_position = 0;
        horizontalThumbnailRecycler = findViewById(R.id.thumbnialRecycler);
        postThumbnailRecycler = findViewById(R.id.postThumbnailRecycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        final LinearLayoutManager layoutManagerPost = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        postAdapter = new PostThumbnailAdapter(modelListPost);
        horizontalThumbnailRecycler.setLayoutManager(layoutManager);

        horizontalThumbnailRecycler.setItemAnimator(new DefaultItemAnimator());

        postThumbnailRecycler.setItemAnimator(new DefaultItemAnimator());
        postThumbnailRecycler.setLayoutManager(layoutManagerPost);
        horizontalThumbnailRecycler.setAdapter(mAdapter);
        postThumbnailRecycler.setAdapter(postAdapter);


        sharedPreferencesForColor = getSharedPreferences("colorpref", MODE_PRIVATE);
        colorEditor = sharedPreferencesForColor.edit();
        rgbTextView = findViewById(R.id.rgbValue);
        rgb = new int[25][3];
        footerLayout = findViewById(R.id.footerLayout);
        receivedBundle = getIntent().getExtras();
        idb = receivedBundle.getInt("idb");


        sharedPreferencesForFlag = getSharedPreferences("flagpref", MODE_PRIVATE);

        sharedPreferences = getSharedPreferences("my", MODE_PRIVATE);
        flagEditor = sharedPreferencesForFlag.edit();
        String value = sharedPreferencesForFlag.getString("flag", "defaultValue");
        flagEditor.putString("flag", "no");
        flagEditor.commit();
        getSupportActionBar().setTitle("Thread");


        footerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rr = rgb[final_position][0];
                gg = rgb[final_position][1];
                bb = rgb[final_position][2];
                if (footer_flag == 0) {
                    color = Color.argb(255, rr, gg, bb);
                    colorEditor.putInt("" + idb, color);
                    colorEditor.commit();
                    footer_flag = 1;
                    arrowEditor.putInt("arrowflag", idb);
                    arrowEditor.commit();
                    footerLayout.setBackgroundColor(getResources().getColor(R.color.white));
                    rgbResult.setText("This color selected!");
                }

            }
        });


        for (int i = 0; i < 25; i++) {
            Random rnd = new Random();


            rr = rnd.nextInt(256);
            gg = rnd.nextInt(256);
            bb = rnd.nextInt(256);
            rgb[i][0] = rr;
            rgb[i][1] = gg;
            rgb[i][2] = bb;

            color = Color.argb(255, rr, gg, bb);
            Model mode = new Model(color);
            data.add(mode);
            modelListPost.add(mode);
            modelList.add(mode);
            postAdapter.notifyDataSetChanged();

        }
        mAdapter = new ThumbnailAdapter(getApplicationContext(), data, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                postThumbnailRecycler.scrollToPosition(position);
                final_position = position;

                rr = rgb[position][0];
                gg = rgb[position][1];
                bb = rgb[position][2];
                rgbTextView.setText("RGB value is(" + rr + "," + gg + "," + bb + ")");
            }
        });
        horizontalThumbnailRecycler.setAdapter(mAdapter);
        rr = rgb[0][0];
        gg = rgb[0][1];
        bb = rgb[0][2];
        rgbTextView.setText("RGB value is(" + rr + "," + gg + "," + bb + ")");

        postThumbnailRecycler.setOnScrollListener(new RecyclerView.OnScrollListener() {
            int ydy = 0;

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int offset = dy - ydy;
                ydy = dy;

                if (flag_position != -1) {
                    rr = rgb[flag_position][0];
                    gg = rgb[flag_position][1];
                    bb = rgb[flag_position][2];
                    color = Color.argb(255, rr, gg, bb);
                    hexTextView.setBackgroundColor(color);
                    hexTextView.setText(null);
                }

            }
        });

        ItemClickSupport.addTo(postThumbnailRecycler)
                .setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        final_position = position;
                        rr = rgb[position][0];
                        gg = rgb[position][1];
                        bb = rgb[position][2];
                        rgbTextView.setText("RGB value is(" + rr + "," + gg + "," + bb + ")");

                    }

                    @Override
                    public void onItemDoubleClicked(RecyclerView recyclerView, int position, View v) {

                        hexTextView = v.findViewById(R.id.postthumnailImageRecycler);

                        flag_position = position;
                        rr = rgb[position][0];
                        gg = rgb[position][1];
                        bb = rgb[position][2];
                        final String hex = String.format("#%02x%02x%02x", rr, gg, bb).toUpperCase();
                        hexTextView.setText("HEX value is " + hex);

                        hexTextView.animate().rotationY(90f).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                hexTextView.setBackground(getResources().getDrawable(R.drawable.textview_background));
                                hexTextView.setRotationY(270);
                                hexTextView.animate().rotationY(360f).setListener(null);
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {
                            }
                        });

                    }
                });


    }

    @Override
    protected void onResume() {
        super.onResume();
        horizontalThumbnailRecycler.scrollToPosition(0);
    }

    @Override
    public void onBackPressed() {
        Bundle bundle = new Bundle();
        bundle.putString("flag", "thread");
        Intent mIntent = new Intent();
        mIntent.putExtras(bundle);
        setResult(RESULT_OK, mIntent);
        super.onBackPressed();
    }
}
