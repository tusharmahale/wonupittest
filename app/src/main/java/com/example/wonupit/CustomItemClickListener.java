package com.example.wonupit;

import android.view.View;

public interface CustomItemClickListener {
     void onItemClick(View v, int position);
}
