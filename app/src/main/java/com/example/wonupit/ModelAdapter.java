package com.example.wonupit;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.media.midi.MidiOutputPort;
import android.opengl.Visibility;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Random;

import static android.content.Context.MODE_PRIVATE;

public class ModelAdapter extends RecyclerView.Adapter<ModelAdapter.MyViewHolder> {
    SQLiteDatabase mDatabase;
    SharedPreferences sharedPreferencesForArrow;
    SharedPreferences.Editor arrowEditor;
    private List<Model> moviesList;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title,shortText,time,actionTextView;
        ImageView arrowview,watchImageView;
        View cardLayout;
        Context ct;

        public MyViewHolder(final View view, final Context activity) {
            super(view);
            ct = activity;
            sharedPreferencesForArrow = ct.getSharedPreferences("arrow",MODE_PRIVATE);

            arrowEditor = sharedPreferencesForArrow.edit();
//            arrowEditor.putInt("arrowflag",-1);
//            arrowEditor.commit();

            mDatabase = activity.openOrCreateDatabase("dt.db", MODE_PRIVATE, null);
            title = (TextView) view.findViewById(R.id.textView);
            shortText = view.findViewById(R.id.textView2);
            time = view.findViewById(R.id.textView3);
            cardLayout = view.findViewById(R.id.cardLayout);
            arrowview = view.findViewById(R.id.arrowView);
            actionTextView = view.findViewById(R.id.textView4);
            watchImageView = view.findViewById(R.id.imageView2);
            arrowview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(activity, "w" + v.getId(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(activity, Thread_Activity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("idb",v.getId());
                    intent.putExtras(bundle);
                    activity.startActivity(intent);



                }
            });


        }
    }


    public ModelAdapter(List<Model> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.post_card_layout, parent, false);
        Context c = parent.getContext();
        return new MyViewHolder(itemView, c);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Model movie = moviesList.get(position);
        holder.arrowview.setId(movie.getId());
        int arrowflag = sharedPreferencesForArrow.getInt("arrowflag",-1);
        if(arrowflag == position)
        {
            holder.arrowview.setVisibility(View.INVISIBLE);
            holder.actionTextView.setText("Action taken");
            arrowEditor.putInt("arrowflag",-1);
            arrowEditor.commit();

        }

        Random rn = new Random();
        int color = Color.argb(255, rn.nextInt(256), rn.nextInt(256), rn.nextInt(256));

        GradientDrawable gd = new GradientDrawable(GradientDrawable.Orientation.BL_TR, new int[]{0xFF000000, movie.getColor()});
        holder.cardLayout.setBackground(gd);
        holder.title.setText(movie.getTitle());
        holder.time.setText(movie.getTime());
        holder.shortText.setText(movie.getShortText());

        String cordString = movie.getTime();
        if(cordString.substring((cordString.length()-3),cordString.length()).equals("GMT"))
        {
            holder.time.setTextColor(holder.ct.getResources().getColor(R.color.gray));
            holder.watchImageView.setImageDrawable(holder.ct.getDrawable(R.drawable.timer_gray_500));

        }
        else  if(cordString.substring((cordString.length()-3),cordString.length()).equals("IST"))
        {
            holder.time.setTextColor(holder.ct.getResources().getColor(R.color.red));
            holder.watchImageView.setImageDrawable(holder.ct.getDrawable(R.drawable.timer_red));

        }
       else if(cordString.substring((cordString.length()-3),cordString.length()).equals("EST"))
        {
            holder.time.setTextColor(holder.ct.getResources().getColor(R.color.white));
            holder.watchImageView.setImageDrawable(holder.ct.getDrawable(R.drawable.timer_white));

        }


    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(Model item);

    }

}