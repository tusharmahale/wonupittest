package com.example.wonupit;

import android.graphics.drawable.GradientDrawable;

public class Model {
String title;
    String shortText;
    String zone, time;

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    int color;
    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

int id;
public Model(){}
public Model(int color)
{
    this.color = color;
}

public Model(String title,String shortText,String time,int color,int id)
{this.color = color;
    this.title = title;
    this.id = id;
    this.shortText = shortText;
    this.time = time;
}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
