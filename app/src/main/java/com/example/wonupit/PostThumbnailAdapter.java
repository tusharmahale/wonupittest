package com.example.wonupit;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class PostThumbnailAdapter extends RecyclerView.Adapter<PostThumbnailAdapter.MyViewHolder> {

    private List<Model> moviesList;
    private RecyclerViewClickListener mListener;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, shortText, time;
        TextView arrowview, watchImageView;
        View cardLayout;
        Context ct;

        public MyViewHolder(final View view, final Context activity) {
            super(view);
            ct = activity;

            arrowview = view.findViewById(R.id.postthumnailImageRecycler);
view.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Toast.makeText(ct,"Hello",Toast.LENGTH_LONG).show();
    }
});

        }
        public void onClick(View view) {
            mListener.onClick(view, getAdapterPosition());
        }
    }


    public PostThumbnailAdapter(List<Model> moviesList) {
        this.moviesList = moviesList;
    }

    @Override
    public PostThumbnailAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.postthumbnailimage, parent, false);
        Context c = parent.getContext();
        return new PostThumbnailAdapter.MyViewHolder(itemView, c);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int i) {
        Model movie = moviesList.get(i);
        ColorDrawable colorDrawable = new ColorDrawable(movie.getColor());
        holder.arrowview.setBackground(colorDrawable);

    }





    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(Model item);

    }

    public interface RecyclerViewClickListener {

        void onClick(View view, int position);
    }




}